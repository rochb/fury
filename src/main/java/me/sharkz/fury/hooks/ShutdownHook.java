package me.sharkz.fury.hooks;

import me.sharkz.fury.utils.FuryUtil;

public class ShutdownHook implements Runnable {

    @Override
    public void run() {
        FuryUtil.exit("user");
    }
}
