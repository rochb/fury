package me.sharkz.fury.events;

public class FuryEvent implements Cancellable {

    private String name = null;
    private boolean cancelled;

    public FuryEvent(String name) {
        this.name = name;
        this.cancelled = false;
    }

    public FuryEvent() {
        this.name = this.getClass().getSimpleName();
        this.cancelled = false;
    }

    /**
     * Get event name
     * @return String
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean isCancelled) {
        this.cancelled = isCancelled;
    }
}
