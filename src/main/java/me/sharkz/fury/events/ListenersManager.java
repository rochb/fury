package me.sharkz.fury.events;

import me.sharkz.fury.Fury;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ListenersManager {

    private List<EventListener> listeners = new ArrayList<>();

    /**
     * Register event listener
     * @param eventListener Listener
     */
    public void register(EventListener eventListener){
        if(listeners.contains(eventListener)) Fury.logger.warn("Event listener " + eventListener.getClass().getName() + " is already registered !");
        else {
            listeners.add(eventListener);
            if(listeners.contains(eventListener))
                Fury.logger.info("Event listener " + eventListener.getClass().getName() + " has been enabled !");
        }
    }

    /**
     * Call an event
     * @param event Event to call
     */
    public void call(FuryEvent event){
        listeners.forEach(eL -> {
            try {
                execute(eL, event);
            } catch (InvocationTargetException | IllegalAccessException e) {
                Fury.logger.error("Cannot call event " + event.getName() + " : " + e.getMessage());
                e.printStackTrace();
            }
        });
    }

    /**
     * Execute event on event lister
     * @param listener Event listener
     * @param event Event
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    private void execute(EventListener listener, FuryEvent event) throws InvocationTargetException, IllegalAccessException {
        if(event.isCancelled()) return;
        for(Method method : listener.getClass().getMethods())
            if (method.isAnnotationPresent(EventHandler.class))
                method.invoke(listener, event);
    }

    /**
     * Get all registered events listeners
     * @return List<EventListener>
     */
    public List<EventListener> getListeners() {
        return listeners;
    }
}
