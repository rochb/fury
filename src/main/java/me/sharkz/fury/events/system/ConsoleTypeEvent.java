package me.sharkz.fury.events.system;

import me.sharkz.fury.events.FuryEvent;

public class ConsoleTypeEvent extends FuryEvent {

    private final String input;

    public ConsoleTypeEvent(String input) {
        super("ConsoleTypeEvent");
        this.input = input;
    }

    /**
     * Get console input
     * @return String
     */
    public String getInput() {
        return input;
    }
}
