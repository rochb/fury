package me.sharkz.fury.events;

public interface Cancellable {

    /**
     * Get event state
     * @return boolean
     */
    boolean isCancelled();

    /**
     * Cancel event
     * @param isCancelled state
     */
    void setCancelled(boolean isCancelled);

}
