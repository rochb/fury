package me.sharkz.fury;

import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import me.sharkz.fury.events.ListenersManager;
import me.sharkz.fury.hooks.ShutdownHook;
import me.sharkz.fury.modules.ModulesManager;
import me.sharkz.fury.modules.system.ConsoleModule;
import me.sharkz.fury.modules.system.DownloadModule;
import me.sharkz.fury.storage.Creditals;
import me.sharkz.fury.storage.gson.GsonManager;
import org.apache.log4j.Logger;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramLoginResult;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.nio.file.Paths;
import java.util.Map;

public class Fury {

    // Java Utils
    private static Fury instance;
    public static final Logger logger = Logger.getLogger(Fury.class);

    // Insta4j
    protected final Creditals creditals;
    private final Instagram4j instagram;

    // Storage
    private final GsonManager gsonManager;
    private final File dataFolder;
    private final File modulesFolder;

    // Modules
    private final ModulesManager modulesManager;

    // Events
    private final ListenersManager listenersManager;

    public Fury() {
        instance = this;
        // Listen to shutdown
        Runtime.getRuntime().addShutdownHook(new Thread(() -> new ShutdownHook().run()));
        // Define datafolder
        dataFolder = new File(Paths.get("").toAbsolutePath().toString(), "Fury");
        if (!dataFolder.exists()) dataFolder.mkdir();
        // Define
        modulesFolder = new File(dataFolder, "modules");
        if (!modulesFolder.exists()) modulesFolder.mkdir();
        // Instance gson manager
        gsonManager = new GsonManager(this);
        // Get instagram creditals
        creditals = (Creditals) gsonManager.add(new Creditals());
        // Connect to instagram
        gsonManager.load();
        instagram = setup();

        // Instance ModulesManager
        modulesManager = new ModulesManager(instagram);
        // Instance Listeners Manager
        listenersManager = new ListenersManager();

        // Call preInit function
        preInit();

        // Call init function
        init();

        // Call post init function
        postInit();

        gsonManager.load();
    }

    private void preInit() {
        modulesManager.register(new ConsoleModule(this));
        modulesManager.register(new DownloadModule(this));
    }

    private void init() {

    }

    private void postInit() {
    }


    /**
     * Login to instagram account
     *
     * @return Instagram4j
     */
    private Instagram4j setup() {
        logger.info("Hooking into instagram ...");
        Instagram4j i = Instagram4j.builder().username(creditals.getInstagramUsername())
                .password(creditals.getInstagramPassword()).build();
        i.setup();
        InstagramLoginResult rs = null;
        try {
            rs = i.login();
        } catch (IOException e) {
            logger.error("Cannot hook into instagram" + e.getMessage());
            e.printStackTrace();
        }

        if (rs != null && rs.getStatus().equals("ok")) logger.info("Hooked into instagram !");
        else {
            logger.fatal("Cannot hook into instagram, stopping all the services !");
            System.exit(0);
        }
        return i;
    }

    /**
     * Get data folder
     *
     * @return file
     */
    public File getDataFolder() {
        return dataFolder;
    }

    public File getModulesFolder() {
        return modulesFolder;
    }

    /**
     * Get Gson Builder
     *
     * @return GsonBuilder
     */
    public static GsonBuilder getGsonBuilder() {
        return new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE);
    }

    /**
     * Get gson builder
     *
     * @param adapters Gson adapters list
     * @return GsonBuilder
     */
    public static GsonBuilder getGsonBuilder(Map<Type, TypeAdapter> adapters) {
        GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().serializeNulls()
                .excludeFieldsWithModifiers(Modifier.TRANSIENT, Modifier.VOLATILE);
        adapters.forEach(gsonBuilder::registerTypeAdapter);
        return gsonBuilder;
    }

    public static void main(String[] args) {
        new Fury();
    }

    /* Getters & Setters */

    /**
     * Get Fury instance
     *
     * @return Fury
     */
    public static Fury getInstance() {
        return instance;
    }

    /**
     * Get Instagram4j instance
     *
     * @return Instagram4j
     */
    public Instagram4j getInstagram() {
        return instagram;
    }

    /**
     * Get gson manager
     *
     * @return GsonManager
     */
    public GsonManager getGsonManager() {
        return gsonManager;
    }

    public ModulesManager getModulesManager() {
        return modulesManager;
    }

    public ListenersManager getListenersManager() {
        return listenersManager;
    }
}
