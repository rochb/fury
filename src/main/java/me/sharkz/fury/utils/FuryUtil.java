package me.sharkz.fury.utils;

import me.sharkz.fury.Fury;

public class FuryUtil {

    public static void exit(String requester){
        Fury.logger.info("Shutdown requested by " + requester);
        if(Fury.getInstance() != null && Fury.getInstance().getModulesManager() != null)
            Fury.getInstance().getModulesManager().unload();
        if(Fury.getInstance() != null && Fury.getInstance().getGsonManager() != null){
            Fury.logger.info("Saving datas...");
            Fury.getInstance().getGsonManager().save();
            Fury.logger.info("Datas saved !");
        }
    }
}
