package me.sharkz.fury.utils;

import me.sharkz.fury.Fury;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.InstagramFollowRequest;
import org.brunocvcunha.instagram4j.requests.InstagramGetUserFollowersRequest;
import org.brunocvcunha.instagram4j.requests.InstagramSearchUsernameRequest;
import org.brunocvcunha.instagram4j.requests.InstagramUnfollowRequest;
import org.brunocvcunha.instagram4j.requests.payload.InstagramGetUserFollowersResult;
import org.brunocvcunha.instagram4j.requests.payload.InstagramSearchUsernameResult;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUser;

import java.io.IOException;

public class UsersUtil {

    private static final Instagram4j insta = Fury.getInstance().getInstagram();

    /**
     * Returns user by username
     *
     * @param username username
     * @return InstagramUser
     */
    public static InstagramUser getByName(String username){
        InstagramSearchUsernameResult rs = null;
        try {
            rs = insta.sendRequest(new InstagramSearchUsernameRequest(username));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(rs != null && rs.getUser() != null) return rs.getUser();
        return null;
    }

    /**
     * Returns true if user exists
     *
     * @param username username
     * @return boolean
     */
    public static boolean exists(String username){
        return getByName(username) != null;
    }

    /**
     * Returns true if user is followed
     *
     * @param user to check
     * @return boolean
     */
    public static boolean isFollowed(InstagramUser user){
        InstagramGetUserFollowersResult rs = null;
        try {
            rs = insta.sendRequest(new InstagramGetUserFollowersRequest(insta.getUserId()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(rs == null) return false;
        return rs.getUsers().stream().anyMatch(u -> u.getPk() == user.getPk());
    }

    /**
     * Follow user
     *
     * @param user user to follow
     */
    public static void follow(InstagramUser user){
        try {
            insta.sendRequest(new InstagramFollowRequest(user.getPk()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unfollow user
     *
     * @param user user to unfollow
     */
    public static void unfollow(InstagramUser user){
        try {
            insta.sendRequest(new InstagramUnfollowRequest(user.getPk()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
