package me.sharkz.fury.storage;

import me.sharkz.fury.storage.gson.Persist;
import me.sharkz.fury.storage.gson.Saveable;

import java.util.Map;

public class Creditals implements Saveable {

    private static Map<String, String> instagram = Map.of("username", "myinstagramusername", "password", "myinstagrampassword");

    public String getInstagramUsername(){
        return instagram.get("username");
    }

    public String getInstagramPassword(){
        return instagram.get("password");
    }

    @Override
    public void save(Persist persist) {}

    @Override
    public void load(Persist persist) {
        persist.loadOrSaveDefault(this, Creditals.class, "creditals");
    }
}
