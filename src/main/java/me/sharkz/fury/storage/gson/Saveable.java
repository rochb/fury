package me.sharkz.fury.storage.gson;

public interface Saveable {
	
	void save(Persist persist);
	void load(Persist persist);
}
