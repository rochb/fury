package me.sharkz.fury.storage.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.sharkz.fury.Fury;

import java.util.ArrayList;
import java.util.List;

public class GsonManager {

    private final Gson gson;
    private final Persist persist;
    private final List<Saveable> savers = new ArrayList<>();

    public GsonManager(Fury main) {
        this.gson = Fury.getGsonBuilder().create();
        this.persist = new Persist(main, this);
    }

    public GsonManager(Fury main, GsonBuilder gsonBuilder) {
        this.gson = gsonBuilder.create();
        this.persist = new Persist(main, this);
    }

    public Saveable add(Saveable saveable){
        savers.add(saveable);
        return saveable;
    }

    public void load(){
        savers.forEach(s -> s.load(persist));
    }

    public void save(){
        savers.forEach(s -> s.save(persist));
    }

    public Gson getGson() {
        return gson;
    }

    public Persist getPersist() {
        return persist;
    }

    public List<Saveable> getSavers() {
        return savers;
    }
}
