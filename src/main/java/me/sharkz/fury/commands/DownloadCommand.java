package me.sharkz.fury.commands;

import me.sharkz.fury.Fury;
import me.sharkz.fury.modules.system.DownloadModule;
import me.sharkz.fury.utils.UsersUtil;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUser;

import java.util.Arrays;
import java.util.List;

public class DownloadCommand implements Executable {

    @Override
    public void execute(Instagram4j instagram4j, String[] args) {
        DownloadModule dlm = (DownloadModule) Fury.getInstance().getModulesManager().getModule("DownloadModule");
        if(dlm == null){
            Fury.logger.error("Could not find download module !");
            return;
        }
        if(args.length < 1){
            Fury.logger.warn("Please specify user...");
            return;
        }
        InstagramUser target = UsersUtil.getByName(args[0]);
        if(target == null){
            Fury.logger.warn("@" + args[0] + " dosen't exists !");
            return;
        }
        dlm.downloadUserMedias(target);
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("dl", "d");
    }

    @Override
    public String getDescription() {
        return "Download all medias from instagram's user.";
    }
}
