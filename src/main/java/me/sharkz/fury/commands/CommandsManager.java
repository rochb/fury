package me.sharkz.fury.commands;

import me.sharkz.fury.Fury;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.HashMap;
import java.util.Map;

public class CommandsManager {

    private Map<String, Executable> commands = new HashMap<>();
    private final Instagram4j instagram4j;

    public CommandsManager(Instagram4j instagram4j) {
        this.instagram4j = instagram4j;
    }

    /**
     * Register a command
     * @param cmd Cmd name
     * @param command Command
     * @return executable
     */
    public Executable register(String cmd, Executable command) {
        if (commands.containsKey(cmd)) {
            Fury.logger.warn("Command " + cmd + " is already registered !");
            return null;
        } else if (commands.containsValue(command)) {
            Fury.logger.warn("Command " + command.getClass().getName() + " is already registered !");
            return null;
        }
        commands.put(cmd, command);
        Fury.logger.info("Command " + cmd + " has been registered !");
        return command;
    }

    /**
     * Check if command exists
     * @param cmd name
     * @return boolean
     */
    public boolean exists(String cmd){
        return commands.keySet().stream().anyMatch(s -> s.equalsIgnoreCase(cmd)) || commands.values().stream().anyMatch(executable -> executable.getAliases().contains(cmd));
    }

    /**
     * Execute command
     * @param cmd Cmd name
     */
    public void executeCmd(String cmd, String[] args){
        if(!exists(cmd)){
            Fury.logger.warn("Command " + cmd + " isn't registered... Try help to shop help menu");
            return;
        }
        if(commands.keySet().stream().anyMatch(s -> s.equalsIgnoreCase(cmd)))
            commands.get(cmd).execute(instagram4j, args);
        else if(commands.values().stream().anyMatch(executable -> executable.getAliases().contains(cmd)))
            commands.values().stream().filter(executable -> executable.getAliases().contains(cmd))
                    .findFirst().ifPresent(executable -> executable.execute(instagram4j, args));
    }

    /**
     * Get commands map
     * @return Map<String, Executable>
     */
    public Map<String, Executable> getCommands() {
        return commands;
    }
}
