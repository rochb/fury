package me.sharkz.fury.commands;

import me.sharkz.fury.Fury;
import me.sharkz.fury.modules.system.ConsoleModule;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.Arrays;
import java.util.List;

public class HelpCommand implements Executable {

    @Override
    public void execute(Instagram4j instagram4j, String[] args) {
        CommandsManager commandsManager = ((ConsoleModule)Fury.getInstance().getModulesManager().getModule("FuryConsole")).getCommandsManager();
        if(commandsManager == null){
            Fury.logger.error("Internal error, could not find Console Module !");
            return;
        }
        System.out.println("========= [ FURY ] =========\n");
        commandsManager.getCommands().forEach((s, executable) -> System.out.println(s + " - " + executable.getDescription()));
        System.out.println("\n============================");
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("?", "h");
    }

    @Override
    public String getDescription() {
        return "Show help menu";
    }
}
