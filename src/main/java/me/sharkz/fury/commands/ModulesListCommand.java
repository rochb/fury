package me.sharkz.fury.commands;

import me.sharkz.fury.Fury;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.ArrayList;
import java.util.List;

public class ModulesListCommand implements Executable {

    @Override
    public void execute(Instagram4j instagram4j, String[] args) {
        StringBuilder sb = new StringBuilder("Enabled modules : \n");
        Fury.getInstance().getModulesManager().getEnabledModules().forEach(furyModule -> sb.append(furyModule.getName()).append(", "));
        Fury.logger.info(sb.toString());
    }

    @Override
    public List<String> getAliases() {
        return new ArrayList<>();
    }

    @Override
    public String getDescription() {
        return "Get enabled modules list.";
    }
}
