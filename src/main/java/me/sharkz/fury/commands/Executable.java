package me.sharkz.fury.commands;

import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.List;

public interface Executable {

    void execute(Instagram4j instagram4j, String[] args);

    List<String> getAliases();

    String getDescription();
}
