package me.sharkz.fury.commands;

import me.sharkz.fury.Fury;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.Arrays;
import java.util.List;

public class UptimeCommand implements Executable{

    private final long start;

    public UptimeCommand() {
        this.start = System.currentTimeMillis();
    }

    @Override
    public void execute(Instagram4j instagram4j, String[] args) {
        Fury.logger.info("Uptime : " + ( System.currentTimeMillis() - start) + " ms");
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("up");
    }

    @Override
    public String getDescription() {
        return "Get bot's uptime";
    }
}
