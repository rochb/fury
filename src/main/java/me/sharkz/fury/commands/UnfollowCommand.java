package me.sharkz.fury.commands;

import me.sharkz.fury.Fury;
import me.sharkz.fury.utils.UsersUtil;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUser;

import java.util.ArrayList;
import java.util.List;

public class UnfollowCommand implements Executable{

    @Override
    public void execute(Instagram4j instagram4j, String[] args) {
        InstagramUser target = UsersUtil.getByName(args[0]);
        if(target == null){
            Fury.logger.warn("@" + args[0] + " dosen't exists !");
            return;
        }
        if(!UsersUtil.isFollowed(target)){
            Fury.logger.warn("@" + args[0] + " is not followed !");
            return;
        }
        UsersUtil.unfollow(target);
        Fury.logger.info("Not longer following @" + target.getUsername());
    }

    @Override
    public List<String> getAliases() {
        return new ArrayList<>();
    }

    @Override
    public String getDescription() {
        return "Unfollow specific user";
    }
}
