package me.sharkz.fury.modules;

import me.sharkz.fury.Fury;
import me.sharkz.fury.commands.Executable;
import me.sharkz.fury.modules.system.ConsoleModule;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.io.File;

public abstract class FuryModule {

    protected final Fury fury;
    protected final Instagram4j insta;
    private final String name;
    private final String author;
    private final String version;
    private final String website;
    private final String description;

    private final File dataFolder;
    private boolean enabled;

    public FuryModule(Fury fury, String name, String author, String version, String website, String description) {
        this.fury = fury;
        this.insta = fury.getInstagram();
        this.name = name;
        this.author = author;
        this.version = version;
        this.website = website;
        this.description = description;
        this.enabled = false;
        this.dataFolder = getDF();
    }

    public FuryModule(Fury fury, String name, String author, String version, String website) {
        this.fury = fury;
        this.insta = fury.getInstagram();
        this.name = name;
        this.author = author;
        this.version = version;
        this.website = website;
        this.description = "I'm a silly module :)";
        this.dataFolder = getDF();
    }

    public FuryModule(Fury fury, String name, String author, String version) {
        this.fury = fury;
        this.insta = fury.getInstagram();
        this.name = name;
        this.author = author;
        this.version = version;
        this.website = "www.roch-blondiaux.com";
        this.description = "I'm a silly module :)";
        this.dataFolder = getDF();
    }

    public FuryModule(Fury fury, String name, String author) {
        this.fury = fury;
        this.insta = fury.getInstagram();
        this.name = name;
        this.author = author;
        this.version = "1.0";
        this.website = "www.roch-blondiaux.com";
        this.description = "I'm a silly module :)";
        this.dataFolder = getDF();
    }

    public FuryModule(Fury fury, String name) {
        this.fury = fury;
        this.insta = fury.getInstagram();
        this.name = name;
        this.author = "Unknown";
        this.version = "1.0";
        this.website = "www.roch-blondiaux.com";
        this.description = "I'm a silly module :)";
        this.dataFolder = getDF();
    }

    /**
     * Get Module DataFolder
     * @return File
     */
    protected File getDF(){
        File df = new File(Fury.getInstance().getModulesFolder(), name);
        if(!df.exists())
            if(!df.mkdir())
                return null;

        return df;
    }

    /**
     * Called on module enable
     * @param instagram4j Instagram4j instance
     */
    public abstract void onEnable(Instagram4j instagram4j);

    /**
     * Called on module disable
     */
    public abstract void onDisable();

    /**
     * Check if module is valid
     * @return boolean
     */
    public boolean isValid(){
        return name != null && author != null && version != null;
    }

    /**
     * Register command
     * @param cmd Command name
     * @param executable Command class
     */
    public void registerCommand(String cmd, Executable executable){
        if(fury.getModulesManager().exists("FuryConsole")){
            ConsoleModule consoleModule = (ConsoleModule) fury.getModulesManager().getModule("FuryConsole");
            consoleModule.getCommandsManager().register(cmd, executable);
            return;
        }
        Fury.logger.warn("Could not find console module !");
    }

    /* Getters & Setters */
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getVersion() {
        return version;
    }

    public String getWebsite() {
        return website;
    }

    public String getDescription() {
        return description;
    }

    public File getDataFolder() {
        return dataFolder;
    }

    public Instagram4j getInsta() {
        return insta;
    }

    public Fury getFury() {
        return fury;
    }
}
