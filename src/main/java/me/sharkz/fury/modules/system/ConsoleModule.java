package me.sharkz.fury.modules.system;

import me.sharkz.fury.Fury;
import me.sharkz.fury.commands.*;
import me.sharkz.fury.events.system.ConsoleTypeEvent;
import me.sharkz.fury.modules.FuryModule;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.Arrays;

public class ConsoleModule extends FuryModule {

    private Thread thread;
    private CommandsManager commandsManager;

    public ConsoleModule(Fury fury) {
        super(fury, "FuryConsole", "Sharkz", "1.0-alpha", "www.roch-blondiaux.com", "Execute commands & parse args.");
    }

    protected void initThread(){
        thread = new Thread(() -> {
            while(!thread.isInterrupted()){
                String input = System.console().readLine("FURY » ");
                if(input.length() >= 1){
                    ConsoleTypeEvent event = new ConsoleTypeEvent(input);
                    Fury.getInstance().getListenersManager().call(event);
                    String[] tmpArgs = input.split(" ");
                    String cmd = tmpArgs[0];
                    String[] args = Arrays.copyOfRange(tmpArgs, 1, tmpArgs.length);
                    commandsManager.executeCmd(cmd, args);
                }
            }
        });
    }

    @Override
    public void onEnable(Instagram4j instagram4j) {
        commandsManager = new CommandsManager(instagram4j);

        commandsManager.register("help", new HelpCommand());
        commandsManager.register("download", new DownloadCommand());
        commandsManager.register("uptime", new UptimeCommand());
        commandsManager.register("modules", new ModulesListCommand());
        commandsManager.register("follow", new FollowCommand());
        commandsManager.register("unfollow", new UnfollowCommand());
        

        initThread();
        thread.start();
    }

    @Override
    public void onDisable() {
        thread.interrupt();
    }

    public CommandsManager getCommandsManager() {
        return commandsManager;
    }
}
