package me.sharkz.fury.modules.system;

import me.sharkz.fury.Fury;
import me.sharkz.fury.modules.FuryModule;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.InstagramUserFeedRequest;
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedResult;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUser;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class DownloadModule extends FuryModule {

    private File mediasFolder;

    public DownloadModule(Fury fury) {
        super(fury, "DownloadModule", "Sharkz", "1.0", "www.roch-blondiaux.com", "Download all medias from instagram's user.");
    }

    @Override
    public void onEnable(Instagram4j instagram4j) {
        mediasFolder = new File(getDataFolder(), "medias");
        if(!mediasFolder.exists()) mediasFolder.mkdir();
    }

    @Override
    public void onDisable() {

    }

    public File getMediasFolder() {
        return mediasFolder;
    }

    public void downloadUserMedias(InstagramUser instagramUser){
        InstagramFeedResult result = null;
        try {
            result = getInsta().sendRequest(new InstagramUserFeedRequest(instagramUser.getPk()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(result == null || result.getStatus().equals("fail")){
            Fury.logger.error("Cannot get @" + instagramUser.getUsername() + "'s medias");
            return;
        }
        result.getItems().forEach(medias -> {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_kk_mm_ss");
            String name = medias.user.getUsername() + "_" + sdf.format(new Date(medias.taken_at * 1_000L));
            if(medias.getCarousel_media() != null){
                AtomicInteger count = new AtomicInteger(1);
                medias.getCarousel_media().forEach(instagramCarouselMediaItem -> {
                    if(instagramCarouselMediaItem.getVideo_versions() != null){
                        instagramCarouselMediaItem.getVideo_versions().forEach(imageMeta -> {
                            try {
                                downloadImage(imageMeta.getUrl(), name + ".mp4", getMediasFolder().getPath());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    instagramCarouselMediaItem.getImage_versions2().getCandidates().forEach(imageMeta -> {
                        try {
                            downloadImage(imageMeta.getUrl(), name + "_" + count.getAndIncrement() + ".jpg", getMediasFolder().getPath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                });
            }else if(medias.getImage_versions2() != null){
                medias.getImage_versions2().getCandidates().forEach(imageMeta -> {
                    try {
                        downloadImage(imageMeta.getUrl(), name + ".jpg", getMediasFolder().getPath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }else if(medias.getVideo_versions() != null){
                medias.getVideo_versions().forEach(imageMeta -> {
                    try {
                        downloadImage(imageMeta.getUrl(), name + ".mp4", getMediasFolder().getPath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            // TODO : Download video & photos medias
        });
    }

    /**
     * Download image from url
     * @param imageUrl image Url
     * @param name Image name
     * @param destination image folder
     * @throws IOException not found exception
     */
    public void downloadImage(String imageUrl, String name, String destination) throws IOException {
        URL url = new URL(imageUrl);
        String destName = destination + File.separatorChar + name;

        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destName);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) os.write(b, 0, length);

        is.close();
        os.close();
    }

}
