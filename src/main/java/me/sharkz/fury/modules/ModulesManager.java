package me.sharkz.fury.modules;

import me.sharkz.fury.Fury;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ModulesManager {

    private final List<FuryModule> modules = new ArrayList<>();
    private final Instagram4j instagram4j;

    /**
     *
     * @param instagram4j Instagram4j instance
     */
    public ModulesManager(Instagram4j instagram4j) {
        this.instagram4j = instagram4j;
    }

    /**
     * Register & load module
     * @param module Module to register, load & enable
     */
    public void register(FuryModule module){
        if(exists(module.getName()))
            Fury.logger.warn("Module with name " + module.getName() + " has already been registered !");
        else{
            Fury.logger.info("Trying to load module " + module.getName());
            load(module);
        }
    }

    /**
     *
     * @param module Module to load
     */
    private void load(FuryModule module){
        if(module.isValid())
            enableModule(module);
        else
            Fury.logger.warn("Module " + module.getName() + " is not valid !");
    }

    /**
     * Disable all enabled modules
     */
    public void unload(){
        Fury.logger.info("Unloading & disabling all modules !");
        getEnabledModules().forEach(this::disableModule);
        modules.clear();
        Fury.logger.info("All modules have been unloaded & disabled !");
    }


    /**
     * Enable Module
     * @param name Module name
     */
    public void enableModule(String name){
        FuryModule m = getModule(name);
        if(m != null) enableModule(m);
    }

    /**
     * Enable module
     * @param module Module to enable
     */
    public void enableModule(FuryModule module){
        if(module.isEnabled()){
            Fury.logger.warn("Module " + module.getName() + " is already enable !");
            return;
        }
        Fury.logger.info("Loading & enabling module " + module.getName());
        module.onEnable(instagram4j);
        module.setEnabled(true);
        modules.add(module);
        Fury.logger.info("Module " + module.getName() + " has been loaded & enabled !");
    }

    /**
     * Disable module
     * @param name Module name
     */
    public void disableModule(String name){
        FuryModule m = getModule(name);
        if(m != null) disableModule(m);
    }

    /**
     * Disable module
     * @param module Module to disable
     */
    public void disableModule(FuryModule module){
        if(!module.isEnabled()){
            Fury.logger.warn("Module " + module.getName() + " is already disabled !");
            return;
        }
        module.onDisable();
        module.setEnabled(false);
        Fury.logger.info("Module " + module.getName() + " has been disabled !");
    }

    /**
     * Reload module
     * @param name Module name
     */
    public void reloadModule(String name){
        disableModule(name);
        enableModule(name);
    }

    /**
     * Reload module
     * @param module Module to reload
     */
    public void reloadModule(FuryModule module){
        disableModule(module);
        enableModule(module);
    }

    /**
     * Get Module by name
     * @param name Module name
     * @return FuryModule
     */
    public FuryModule getModule(String name){
        Optional<FuryModule> module = modules.stream().filter(m -> m.getName().equalsIgnoreCase(name)).findFirst();
        if(module.isEmpty()){
            Fury.logger.warn("Module with name " + name + " isn't loaded !");
            return null;
        }else return module.get();
    }

    /**
     * Check if module exists
     * @param name Module name
     * @return boolean
     */
    public boolean exists(String name){
        return modules.stream().anyMatch(m -> m.getName().equalsIgnoreCase(name));
    }

    /* Getters & Setters */
    /**
     * Get list of enabled modules
     * @return list
     */
    public List<FuryModule> getEnabledModules(){
        return modules.stream().filter(FuryModule::isEnabled).collect(Collectors.toList());
    }

    /**
     * Get list of all modules
     * @return list
     */
    public List<FuryModule> getModules() {
        return modules;
    }
}
